from datetime import datetime

class Item:

    _DATE_FORMAT="%Y-%m-%d" 

    def __init__(self, id, name, description, duedate, lastmmodified, status = 'To Do'):
        self.id = id
        self.name = name
        self.description = description
        self.duedate = duedate
        self.lastmodified = lastmmodified
        self.status = status

    @classmethod
    def from_mongo_database(cls, record):
        return cls(record['_id'], record['name'], record['description'], Item.safe_get_date_str_from_timestamp(record['due']), record['dateLastActivity'], record['status'])  

    @staticmethod
    def safe_get_date(string_date):
        # Trim off the "Z" as datetime doesn't support it
        if string_date != None:
            return datetime.fromisoformat(string_date[:-1]).date()
            
        return None
    

    @staticmethod
    def safe_get_date_str_from_timestamp(date):
        # Mongo db only supports datetime , not date. we only need to show date, so formatting this here.
        if date != None and date != '':
            return date.strftime(Item._DATE_FORMAT)
        return None
    
    @staticmethod
    def safe_get_date_from_str(date):
        # Handle user input for dates
        if date != None and date != '':
            return  datetime.strptime(date, Item._DATE_FORMAT)
        return None

    '''
        Helpful string representation of the object - useful when debugging.
    '''
    def __str__(self):
     return f"Item: [ id = {self.id} , name = {self.name}, description = {self.description}, duedate = {self.duedate}, lastmodified = {self.lastmodified}, status = {self.status} ]"