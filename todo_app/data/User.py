from flask_login import UserMixin
from typing import List

class User(UserMixin):
    def __init__(self,mongoid, id, roles: List):
        self.mongoid = mongoid
        self.id = id
        self.roles = roles
      

    def hasrole(self, rolename : str):
        if rolename in self.roles:
            return True
        return False
    
    @staticmethod
    def from_mongo_db(json):
        return User(mongoid = json['_id'], id = json['id'] , roles = json['roles'])


    def __str__(self):
     return f"User: [ mongoid = {self.mongoid}, id = {self.id} , roles = {self.roles} ]"

