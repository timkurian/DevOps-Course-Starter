from datetime import datetime, date
import sys


class ViewModel:
    _MIN_DONE_ITEMS = 5
    _DATE_FORMAT="%Y-%m-%d" 
    
    def __init__(self, items):
        self._items = items

    @property
    def items(self):
        return self._items

    @property
    def doing_items(self):
        return [ item for item in self.items if item.status == 'Doing' ]
    
    @property
    def todo_items(self):
        return [ item for item in self.items if item.status == 'To Do' ]
    
    def _get_all_done_items(self):    
        return [ item for item in self.items if item.status == 'Done' ]

    @property
    def done_items(self):
        done_items = self._get_all_done_items()
        if len(done_items) < self._MIN_DONE_ITEMS:
           return done_items 
        else:
            today = date.today().strftime(self._DATE_FORMAT)
            return [ done_item for done_item in done_items if done_item.lastmodified == date.today() ]
    
    @property
    def older_done_items(self):
        done_items = self._get_all_done_items()
        if len(done_items) >= self._MIN_DONE_ITEMS:  
            today = date.today().strftime(self._DATE_FORMAT)
            return [ done_item for done_item in done_items if done_item.lastmodified != date.today() ]
        return []