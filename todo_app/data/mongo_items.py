from datetime import datetime, date
from hashlib import new
from xml.dom import NotFoundErr
from todo_app.data.Item import Item
from todo_app.data.User import User
from bson.objectid import ObjectId
from typing import List
import pymongo

class Mongo_db_service:

    def __init__(self, app_config):
        self.MONGO_DB_URL=app_config['MONGO_DB_URL']
        self.MONGO_DATABASE_NAME=app_config['MONGO_DATABASE_NAME']
        self.client = pymongo.MongoClient(self.MONGO_DB_URL)
        self.database = self.client[self.MONGO_DATABASE_NAME]

    
    def get_items(self):
        response_items = self.database.todos.find()
        items = [Item.from_mongo_database(record) for record in response_items]
        items.sort(key=lambda x: x.status)
        return items

    def get_item(self, id ):
        todo = self.database.todos.find_one({"_id": ObjectId(id)})
        if todo == None:
            raise NotFoundErr(f'The todo item with id { id } does not exist')

        return Item.from_mongo_database(todo)
    
    def save_item(self, id,name,status,duedate,description):
        
        update_query = { "$set" : { "name": name, "status" : status, "due": Item.safe_get_date_from_str(duedate), "description": description, "dateLastActivity":datetime.today() } }
        self.database.todos.update_one({"_id": ObjectId(id)}, update_query)
    
    def remove_item(self, id):
        self.database.todos.delete_one({"_id": ObjectId(id)})

    def add_item(self, name, description,duedate):

        todo = {"name": name,
            "description": description,
            "status" : "To Do",
            "due": Item.safe_get_date_from_str(duedate),
            "dateLastActivity": datetime.today()
        }      
        self.database.todos.insert_one(todo)
    
    
    def get_users(self) -> List[User]:
        db_users = self.database.users.find()
        return [User.from_mongo_db(record) for record in db_users]

    
    def get_user(self, user_id) -> User:
        response_user = self.database.users.find_one({"id":user_id})
        if response_user is None:
            if len(list(self.database.users.find())) == 0:
                # If there are no users, the first one will be created as an admin
                new_user = User(mongoid = None, id=user_id, roles = ['reader', 'writer', 'admin'])
            else:
                new_user = User(mongoid = None, id=user_id, roles = ['reader'])
            self.add_user(new_user)
            saved_user = self.database.users.find_one({"id":user_id})
            return User.from_mongo_db(json = saved_user)

        else:
            return User.from_mongo_db(json = response_user)


    def add_user(self, user: User):
        user = { "id" : user.id,
            "roles": user.roles
            }
        self.database.users.insert_one(user)

    def update_user(self, _id, id, roles):

        update_query = { "$set" : { "id": id, "roles": roles } }
        self.database.users.update_one({"_id": ObjectId(_id)}, update_query)


