import os
from threading import Thread

import pytest
import requests
from dotenv import find_dotenv, load_dotenv
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from todo_app import app
from pathlib import Path
import pymongo


def chrome_options() -> None:
    """Sets chrome options for Selenium.
    Chrome options for headless browser is enabled.
    """
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])
    return chrome_options

   
@pytest.fixture(scope='module')
def app_with_temp_board():

    load_dotenv(find_dotenv(".env"), override=True)
    os.environ['LOGIN_DISABLED'] = 'True'
    create_database()
    # construct the new application
    application = app.create_app()
    # start the app in its own thread.
    thread = Thread(target=lambda: application.run(use_reloader=False))
    thread.daemon = True
    thread.start()
    yield application
    # Tear Down
    thread.join(1)
    destroy_database()


@pytest.fixture(scope="module")
def driver():
    with webdriver.Chrome(options=chrome_options()) as driver:
        yield driver


def create_database():
    # Prefix the live database name with "test" so we can drop it when we're done testing
    os.environ['MONGO_DATABASE_NAME'] = f"test_{os.environ['MONGO_DATABASE_NAME']}"
    

def destroy_database():
    MONGO_DB_URL=os.environ['MONGO_DB_URL']
    database_name = os.environ['MONGO_DATABASE_NAME']
    pymongo.MongoClient(MONGO_DB_URL).drop_database(database_name)



def test_end_to_end(app_with_temp_board,driver ):
    driver.get('http://localhost:5000')
    assert driver.title == 'To-Do App'
    
    # fill in form for todo:
    title = driver.find_element(By.ID, "add_todo_title")
    description = driver.find_element(By.ID, "add_todo_description")
    duedate = driver.find_element(By.ID, "add_todo_duedate")
    submit_button = driver.find_element(By.ID, "add_todo_submit_button")
  
    # fill in form
    title.send_keys("This is a todo")
    description.send_keys("Once upon a time, one had to manually test; then Selenium came to automate it")
    #duedate.send_keys("22/01/2025")
  
    # Submit form
    submit_button.click()
    # Assert a todo has been added
    todo_items = driver.find_element(By.ID, "main_view_todos_in_todo_state")
    rows = todo_items.find_elements(By.TAG_NAME, "tr") # get all of the rows in the table

    assert len(rows) > 1, f"It appears there are no todo's. System was expecting one. {rows}"

    retrieved_title = rows[1].find_elements(By.TAG_NAME, "td")[0] # The first row is the header, so we're expecting two
    retrieved_description = rows[1].find_elements(By.TAG_NAME, "td")[1]
    retrieved_duedate = rows[1].find_elements(By.TAG_NAME, "td")[2]
    retrieved_modify_button = rows[1].find_elements(By.TAG_NAME, "td")[3]

    assert retrieved_title.text == "This is a todo"
    assert retrieved_description.text == "Once upon a time, one had to manually test; then Selenium came to automate it"
    #assert retrieved_duedate.text == "2025-01-22" # Note, date format on the UI is displayed YYYY-MM-DD

    # Now lets move it to doing

    retrieved_modify_button.find_element(By.LINK_TEXT, 'Modify').click()
    
    modify_status = Select(driver.find_element(By.ID, "modify_form_status"))
    modify_submit_button = driver.find_element(By.ID, "modify_form_submit_button")
  
    # Modify status to in progress
    modify_status.select_by_visible_text('Doing')
    modify_submit_button.click()

    # Assert the todo is in Doing state, and no other data has changed
    todo_items = driver.find_element(By.ID, "main_view_todos_in_todo_state")
    todo_rows = todo_items.find_elements(By.TAG_NAME, "tr")

    doing_items = driver.find_element(By.ID, "main_view_todos_in_doing_state")
    rows = doing_items.find_elements(By.TAG_NAME, "tr") # get all of the rows in the table
    
    retrieved_title = rows[1].find_elements(By.TAG_NAME, "td")[0] 
    retrieved_description = rows[1].find_elements(By.TAG_NAME, "td")[1]
    retrieved_duedate = rows[1].find_elements(By.TAG_NAME, "td")[2]
    retrieved_modify_button = rows[1].find_elements(By.TAG_NAME, "td")[3]

    assert len(todo_rows) == 1 # There shouldn't be a todo now, as its in progress, so the only row is the header 
    assert retrieved_title.text == "This is a todo"
    assert retrieved_description.text == "Once upon a time, one had to manually test; then Selenium came to automate it"
    #assert retrieved_duedate.text == "2025-01-22" # Note, date format on the UI is displayed YYYY-MM-DD

    