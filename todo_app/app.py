from flask import Flask, request, redirect, url_for
from flask_login import LoginManager, login_required, login_user, current_user, logout_user,AnonymousUserMixin
import jwt
from flask.templating import render_template
from todo_app.data.ViewModel import ViewModel
from todo_app.flask_config import Config
from todo_app.data.mongo_items import Mongo_db_service
from todo_app.data.User import User
import logging
import requests
import functools


log = logging.getLogger('werkzeug')
log.setLevel(logging.INFO)

def create_app():

    app = Flask(__name__)

    app.config.from_object(Config())
    mongodb_service = Mongo_db_service(app.config)

    login_manager = LoginManager()

    @login_manager.unauthorized_handler
    def unauthenticated():
        connection_string = f"https://gitlab.com/oauth/authorize?client_id={app.config['CLIENT_ID']}&redirect_uri={request.host_url}login/callback&response_type=code&scope=openid"
        return redirect(connection_string)
    
    @app.route('/login/callback', methods=['GET'])
    def handle_callback():
        code = request.args.get('code')
        parameters_form = f"client_id={app.config['CLIENT_ID']}&client_secret={app.config['CLIENT_SECRET']}&code={code}&grant_type=authorization_code&redirect_uri={request.host_url}login/callback"       
        response = requests.post(f'https://gitlab.com/oauth/token?{parameters_form}')
        token = jwt.decode(response.json()['id_token'], options={"verify_signature": False})
        user = mongodb_service.get_user(token['sub'])
        login_user(user)
        
        return redirect('/')


    @login_manager.user_loader
    def load_user(user_id):
        return mongodb_service.get_user(user_id)

    def auth_required(required_role):
        def wrap(func):
            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                has_access = auth_check(required_role)
                if has_access:
                    return func(*args, **kwargs)
                else:
                    return  redirect('/access_denied')
            return wrapper
        return wrap
    
    def auth_check(required_role):
        
        if current_user.is_anonymous:
            return True
        return  current_user.hasrole(required_role)
        

    login_manager.init_app(app)
    
    @app.route('/logout')
    @login_required
    def logout():
        logout_user()
        return render_template('logged_out.html')
    
    @app.route('/', methods=['GET'])
    @login_required
    @auth_required('reader')
    def index():
        item_view_model = ViewModel(mongodb_service.get_items())
        return render_template('index.html', view_model=item_view_model)

    @app.route('/modify/<id>', methods=['GET'])
    @login_required
    @auth_required('writer')
    def modify_todo(id):
        todo = mongodb_service.get_item(id)
        return render_template('modify.html', todo=todo)

    @app.route('/add_todo', methods=['POST'])
    @login_required
    @auth_required('writer')
    def add_todo():
        mongodb_service.add_item(request.form['title'],request.form['description'],request.form['duedate'])
        return redirect('/')

    @app.route('/update_todo', methods=['POST'])
    @login_required
    @auth_required('writer')
    def update_todo(): 

        id = request.form['id']
        name = request.form['name']
        status = request.form['status']
        duedate = request.form['duedate']
        description = request.form['description']
        mongodb_service.save_item(id,name,status,duedate,description)
        return redirect('/')

    @app.route('/remove_todo', methods=['POST'])
    @login_required
    @auth_required('writer')
    def remove_todo():   
        mongodb_service.remove_item(request.form['id'])
        return redirect('/')

    @app.route('/access_denied', methods=['GET'])
    def access_denied():   
        return render_template('access_denied.html')

    @app.route('/admin', methods=['GET'])
    @login_required
    @auth_required('admin')
    def admin():   
        users = mongodb_service.get_users()
        return render_template('administration.html', users=users)
    
    @app.route('/update_users', methods=['POST'])
    @login_required
    @auth_required('admin')
    def update_users():
        mongoid = request.form.get('mongoid')
        id = request.form.get('id')
        reader = request.form.get('Reader') != None
        writer = request.form.get('Writer') != None
        admin = request.form.get('Admin') != None
        roles = []
        if reader is True:
            roles.append('reader')
        if writer is True:
            roles.append('writer')
        if admin is True:
            roles.append('admin')
        mongodb_service.update_user(_id = mongoid, id = id, roles = roles)
        return redirect('/admin')
    return app
