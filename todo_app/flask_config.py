import os


class Config:
    def __init__(self):
        """Base configuration variables."""
        self.SECRET_KEY = os.environ.get('SECRET_KEY')
        if not self.SECRET_KEY:
            raise ValueError("No SECRET_KEY set for Flask application. Did you follow the setup instructions?")
        self.MONGO_DB_URL = os.getenv('MONGO_DB_URL')
        self.MONGO_DATABASE_NAME = os.getenv('MONGO_DATABASE_NAME')
        self.CLIENT_ID = os.getenv('CLIENT_ID')
        self.CLIENT_SECRET = os.getenv('CLIENT_SECRET')
        self.LOGIN_DISABLED = os.getenv('LOGIN_DISABLED') == 'True'