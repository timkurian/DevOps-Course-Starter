
import pytest
from datetime import date
from todo_app.data.ViewModel import ViewModel
from todo_app.data.Item import Item

@pytest.fixture
def viewmodel() -> ViewModel:
    items=[]
    doing = Item( '1', 'Pick Up Laundry', 'Remember to pay a good tip aswell, they do a great job!', date(2022, 4, 24).isoformat(), date.today, 'Doing')
    items.append(doing)
    view = ViewModel(items)
    
    return view


def test_doing_items():

    # Arrange
    items=[]
    viewmodel = ViewModel(items)
    test_date = date(2022, 4, 24).isoformat()
    doing = Item( '1513', 'Check air pressure', 'Low air pressure causes inefficient energy use in vehicles',  test_date, date.today, 'Doing')
    viewmodel.items.append(doing)
    # Act
    doings = viewmodel.doing_items
    # Assert
    assert len(doings) == 1
    assert doing in doings




def test_todo_items(viewmodel: ViewModel):

    # Arrange

    test_date = date(2023, 1, 1).isoformat()
    todo = Item( '1413', 'Solve World Hunger', 'Food waste and world hunger co-exist. Formulate a plan to end this',test_date, date.today, 'To Do')
    viewmodel.items.append(todo)

    # Act
    todos = viewmodel.todo_items
    # Assert
    assert len(todos) == 1
    assert todo in todos
    assert todo.id == '1413'
    assert todo.name == 'Solve World Hunger'
    assert todo.description == 'Food waste and world hunger co-exist. Formulate a plan to end this'
    assert todo.duedate == test_date
    assert todo.status == 'To Do'


def test_done_items(viewmodel: ViewModel):
    
    # Arrange
    test_date = date.today()
    done = Item( '1613', 'Run for the office of US presedent', 'Ask not what your country can do for you', test_date, date.today, 'Done')
    viewmodel.items.append(done)

    # Act
    dones = viewmodel.done_items
    # Assert
    assert len(dones) == 1
    assert done in dones
    assert done.id == '1613'
    assert done.name == 'Run for the office of US presedent'
    assert done.description == 'Ask not what your country can do for you'
    assert done.duedate == test_date
    assert done.status == 'Done'


def test_three_done_aged_items_returns_three(viewmodel: ViewModel):
    
    # Arrange
    for i in range(1,4):
        old_date =  date(1970, 4, i).isoformat()
        done = Item( str(i), f'Note # {{i}}', 'I have written {{i}} note ', old_date, old_date, 'Done')
        viewmodel.items.append(done)
 

    # Act
    dones = viewmodel.done_items
    # Assert
    assert len(dones) == 3


def test_ten_done_items_of_which_nine_are_todays_age_returns_nine(viewmodel: ViewModel):
    
    # Arrange
    for i in range(1,10):
        todays_date = date.today() #.strftime("%Y-%m-%d")
        done = Item( str(i), f'Memoirs # {{i}}', 'I have accomplished {{i}} conquests ', todays_date,todays_date,'Done')
        viewmodel.items.append(done)

    older_date = date(1945, 9, 2).isoformat()
    done = Item( str(i), 'End world war 2', 'War doesn\'t determine who is right, it determines who is left', older_date, older_date, 'Done')
    viewmodel.items.append(done)
    # Act
    done_today = viewmodel.done_items
    done_in_past = viewmodel.older_done_items
    # Assert
    assert len(done_today) == 9
    assert len(done_in_past) == 1

