
import pytest
import pymongo
import os
from bson.objectid import ObjectId
import mongomock
from dotenv import load_dotenv, find_dotenv
from todo_app import app


@pytest.fixture
def client():
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)
    with mongomock.patch(servers=(('fakemongo.com', 27017),)):
        test_app = app.create_app()
        with test_app.test_client() as client:
            yield client

def test_index_page(client):
    # Setup a todo
    collection = pymongo.MongoClient('mongodb://fakemongo.com').todo_database.todos
    single_todo={"_id" : ObjectId("631e0903e2ddce0d0b5a5b97"),
            "name" : "Test adding an item from the UI",
            "description" : "If this works, lets celebrate!",
            "status" : "To Do",
            "due" : None,
             #"due" : {
            #     "date" : 1662854400000
            # },
            "dateLastActivity" : {
                "date" : 1662916371769
            }}


    collection.insert_one(single_todo)

    response = client.get('/')
    assert response.status_code == 200
    assert 'Test adding an item' in response.data.decode()

def test_add_item(client):
    info = {"title": "To Do", "description": "Posting a todo", "duedate" : "2022-08-29"}
    response = client.post('/add_todo', data=info )
    assert response.status_code == 302
    
    # Now confirm its really there:
    response = client.get('/')
    assert response.status_code == 200
    assert 'Posting a todo' in response.data.decode()


def test_update_item(client):
    id = __setup_single_todo()
    response = client.get(f'/modify/{ id }' )
    assert response.status_code == 200


    info = {"id": id, "name": "Test adding an item from the UI", "description": "It worked, Party at 5", "duedate" : "2022-08-29", "status" : 'Done'}
    response = client.post('/update_todo', data=info )
    assert response.status_code == 302
       
    response = client.get('/')
    assert response.status_code == 200
    assert 'Test adding an item from the UI' in response.data.decode()


   
def __setup_single_todo():
    collection = pymongo.MongoClient('mongodb://fakemongo.com').todo_database.todos
    single_todo={
            "name" : "Test adding an item from the UI",
            "description" : "If this works, lets celebrate!",
            "status" : "To Do",
            "due" : None,
             #"due" : {
            #     "date" : 1662854400000
            # },
            "dateLastActivity" : {
                "date" : 1662916371769
            }}


    id = collection.insert_one(single_todo)
    return id.inserted_id
