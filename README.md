# DevOps Apprenticeship: Project Exercise

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://install.python-poetry.org | python3 -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Configuring Mongo 

This project connects to [Monogo DB](https://docs.microsoft.com/en-us/azure/cosmos-db/mongodb/mongodb-introduction) for todo items. This is hosted on Azure as a cosmos db. Ensure you have created this and setup the following in your `.env`

Edit the `.env` file and add the following, replacing the variables as necessary:
```
MONGO_DB_URL=<connection string including username/password>
MONGO_DATABASE_NAME=<database name>
```


## Configuring Oauth

This project uses [gitlab](https://gitlab.com) to handle authenication. Follow the [instructions](https://docs.gitlab.com/ee/integration/oauth_provider.html#user-owned-applications) to register a new application. This app only uses `openid` 

Note, you must set the call back url as `http://localhost:5000/login/callback` for your local testing.

Once you've registed this, store the following variables in .env:

```
CLIENT_ID=<replace with your client id>
CLIENT_SECRET=<replace with your client secret>
```

## Running tests

_If code is worth writing, then its code that's worth testing._

In the root of the project, run the following:
```
poetry run pytest
```

tests are stored under `todo_app/tests`

## Running the App during development

During the development cycle, this section should be followed to view changes immediately.

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.


## Docker

This application has been dockerised and is able to run inside a container

To build the images (one for production, one for development), run the below:

`docker build --tag todo-app .`

```docker
docker build --target development --tag todo-app:dev .
docker build --target production --tag todo-app:prod .
```


### Running for production:

`docker run -p5000:5000 --env-file ./.env -d todo-app:prod  `

Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

### Running during development:

Using docker-compose (includes continuous testing):

Simply execute: 

`docker-compose up`

_Note_ Due to an issue with watchdog unable to receive events inside the container via the mount , the pytests will not run trigger automatically. To work around this, you could login to the running test container runner and touch any .py file.
Alternatively, you could run in a separate shell 

`watchmedo shell-command --patterns="todo_app/*.py" --recursive --ignore-directories --command='poetry run pytest' .`

If you wish to run docker without compose (i.e. building it first, then running it), execute the below:

`docker run -p5100:5100 -e PORT=5100 --env-file ./.env -d --mount type=bind,source="$(pwd)"/todo_app,target=/opt/todo_app todo-app:dev `

Now visit [`http://localhost:5100/`](http://localhost:5100/) in your web browser to view the app.

### Running tests


**Unit and integration tests:**

`poetry run pytest todo_app/tests`

**End to End tests:**

Note, Selenium is used for testing the end to end flow, which creates a board during the test cycle and destroys it afterwards. You will need to install chromedriver, and store this on your path. Once you've done this, execute:

`poetry run pytest todo_app/e2e_tests`

To run this in docker, build this image (for performnce reasons):

`docker build  -t todo_app_test -f selenium_dockerfile.dockerfile .`

and run:

`docker run --env-file .env todo_app_test todo_app/e2e_tests`



## Terraform

Infrastructure is being provision using terraform. The state is stored on azure blob service. 

To apply terraform locally, create a terraform.tfvars and populate the following:

```bash
prefix = "<replace-me>"
client_sercret="<replace-me>"
client_id="<replace-me>"
secret_key = "<replace-me>"
mongo_database = "todo_database"
flask_app = "todo_app/app"

```

Then run `terraform init` and `terraform plan` to view differences between IaC stored in code verses actual tf state

## Application Documentation

Documentation can be found [here](documentation/README.md)

## Production

This repository's pipeline will push changes from the main branch to [Azure](https://timkurian-todo-app-terraform.azurewebsites.net)


