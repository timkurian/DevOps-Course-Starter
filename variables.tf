variable "prefix" {
  description = "The prefix used for all resources in this environment"
}

variable "client_sercret" {
  description = "The gitlab client secret for oauth"
  sensitive   = true
}

variable "client_id" {
  description = "The gitlab client id for oauth"
  sensitive   = true
}

variable "secret_key" {
  description = "Flask secret key for securing sessions"
  sensitive   = true
}

variable "mongo_database" {
  description = "The name of the database in mongo"
}

variable "flask_app" {
  description = "Where to find app.py"
}
