# Todo App Documentation

The following diagrams were made in [app.diagrams.net](https://app.diagrams.net/). These diagrams may be editted with the drawio extension file.

## Context

![Context Diagram](context-diagram.png)


## Container

![Container Diagram](container-diagram.png)

## Component

![Component Diagram](component-diagram.png)