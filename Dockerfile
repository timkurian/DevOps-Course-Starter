FROM  python:3.10-slim-buster as base

RUN apt-get -qq update; apt-get -qq install curl

RUN curl -sSL https://install.python-poetry.org | python3 -

WORKDIR /opt

COPY poetry.toml pyproject.toml poetry.lock /opt/

RUN /root/.local/bin/poetry config virtualenvs.create false --local && /root/.local/bin/poetry install --no-dev --no-root

COPY todo_app /opt/todo_app

# Configure for production

FROM base as production

EXPOSE 5000

ENV PORT=5000

CMD  /root/.local/bin/poetry run gunicorn -w 3 --bind 0.0.0.0:$PORT "todo_app.app:create_app()" 

# Configure for local development

FROM base as development

EXPOSE 5100

CMD  /root/.local/bin/poetry run flask run -h 0.0.0.0 -p 5100

# Configure for tests

FROM base as continuous_test

RUN /root/.local/bin/poetry install

CMD /root/.local/bin/poetry run watchmedo shell-command --patterns="todo_app/*.py" --recursive --ignore-directories --command='echo "File changed: ${watch_src_path}";/root/.local/bin/poetry run pytest' "." 

# Configure for CI pipeline tests

FROM base as ci_pipeline_test

COPY .coveragerc /opt/

RUN /root/.local/bin/poetry install

ENTRYPOINT ["/root/.local/bin/poetry", "run", "pytest" ]
